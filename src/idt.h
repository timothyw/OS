#ifndef IDT_H
#define IDT_H

#include <stdint.h>

typedef struct idt_entry_struct {
	uint16_t offset_1;
	uint16_t selector;
	uint8_t zero;
	uint8_t type_attr;
	uint16_t offset_2;
} __attribute__((packed)) idt_entry_t;

typedef struct idt_ptr_struct {
	uint16_t size;
	idt_entry_t* offset;
} __attribute__((packed)) idt_ptr_t;

typedef struct registers_struct {
	uint32_t edi, esi, ebp, esp, ebx, edx, ecx, eax; // Pushed by pusha.
	uint32_t int_no, int_code;    // Interrupt number and error code (if applicable)
	uint32_t eip, cs, eflags, useresp, ss; // Pushed by the processor automatically.
} registers_t;

typedef void (*isr_handler_t)(registers_t);
void register_interrupt_handler(uint8_t  n, isr_handler_t handler);
#endif
