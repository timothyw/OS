#include "display.h"
#include "idt.h"
#include "io.h"

#define GET_OFFSET_1(x) (((uint32_t) x) & 0xFFFF)
#define GET_OFFSET_2(x) ((((uint32_t) x) >> 16) & 0xFFFF)

#include "isr_defs.h"

idt_entry_t idt_entries[256] = {0};

idt_ptr_t idt_ptr = {.size = sizeof(idt_entry_t)*256-1, .offset=&idt_entries[0]};

isr_handler_t interrupt_handlers[256] = {0};

void init_idt() {
	// Remap the irq table. 32 - 39, 40 - 47
	outb(0x20, 0x11);
	outb(0xA0, 0x11);
	outb(0x21, 0x20);
	outb(0xA1, 0x28);
	outb(0x21, 0x04);
	outb(0xA1, 0x02);
	outb(0x21, 0x01);
	outb(0xA1, 0x01);
	outb(0x21, 0x0);
	outb(0xA1, 0x0);
	// all isrs:
    #include "idt_defs.h"
}

void isr_handler(registers_t regs) {
	//kputs("interrupt: ");
	//print_hex(regs.int_no);
	//kputs(", arg: ");
	//print_hex(regs.int_code);
	//kputs("\n");
	if (regs.int_no > 0x1F && regs.int_no < 0x30) {
		if (regs.int_no >= 0x28)
			outb(0xA0, 0x20);
		outb(0x20, 0x20); 
		
		if (interrupt_handlers[regs.int_no] != 0)
			interrupt_handlers[regs.int_no](regs);
	}
}

void register_interrupt_handler(uint8_t n, isr_handler_t handler) {
	interrupt_handlers[n] = handler;
}
