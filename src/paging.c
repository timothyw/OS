#include "paging.h"
#include "multiboot.h"
#include "display.h"
#include "defs.h"

page_t PAGE_DIR[1024] __attribute__((aligned(0x1000))) = {
        [0] = {.present = 1, .rw = 1, .user = 0, .writet = 0, .cache = 0, .accessed = 0, .zero=0, .size=1, .ignored = 0, .unused = 0, .frame = 0},
        [1 ... (KERN_PAGE-1)] = {0},
        [KERN_PAGE] = {.present = 1, .rw = 1, .user = 0, .writet = 0, .cache = 0, .accessed = 0, .zero=0, .size=1, .ignored = 0, .unused = 0, .frame = 0},
        [(KERN_PAGE+1) ... 1023] = {0}
};

static inline void flush_tlb(uint32_t addr)
{
   asm volatile("invlpg (%0)" ::"r" (addr) : "memory");
}

void map_page(uint32_t phys_page, uint32_t virt_page) {
	PAGE_DIR[virt_page] = (page_t){.present = 1, .rw=1, .user=0, .writet=0, .cache=0, .accessed=0, .zero=0, .size=1, .ignored=0, .unused=0, .frame=phys_page};
}

void init_memory(multiboot_info_t* mbh) {
	page_t zero_page = {0};
	uint32_t virt_page = 0;

	multiboot_memory_map_t* mmap;
	for (mmap = (multiboot_memory_map_t*)mbh->mmap_addr;
		(uint32_t)mmap < mbh->mmap_addr + mbh->mmap_length;
		mmap = (multiboot_memory_map_t*) ((uint32_t) mmap
							+ mmap->size + sizeof (mmap->size))) {
		// ram needs to be usable and pageble in 4mb
		if(mmap->type == 1 && mmap->len > 0x400000 && mmap->addr+mmap->len < 0xFFFFFFFF){
			// skip 0x100000
			uint32_t start = mmap->addr;
			uint32_t end = mmap->addr + mmap->len;
			for (; start < end; start = PAGE_ALIGN(start + 0x400000)) {
				// exclude first page
				if (GET_PAGE(start) != 0x0) {
					if (virt_page == 0) {
						zero_page = (page_t){.present = 1, .rw=1, .user=0, .writet=0, .cache=0, .accessed=0, .zero=0, .size=1, .ignored=0, .unused=0, .frame=GET_PAGE(start)};
					} else {
						map_page(GET_PAGE(start), virt_page);
					}
					virt_page++;
				}
			}
		}
	}
	// print end:
	// = last page
	kputs("last address:");
	print_hex(virt_page*0x400000 - 1);
	kputs("\n");
	PAGE_DIR[0] = zero_page;
	flush_tlb(0x0);
	//TODO: invalidate tlb
	// first phys page  (0x00 - 0x400000) (kernel code) -> 0xe0000000 - ...
	// lower mem not used (x kb)
	// second page -> later page -> 0x0 - ...
	// for main mem
}
