#ifndef KMEM_H
#define KMEM_H

void* memset(void* ptr, uint8_t val, uint32_t len);
void* memcpy(void* dest, const void* src, uint32_t len);
char* strcpy(char* dest, const char* src);
uint32_t strlen(char* str);

#endif
