#include "display.h"
#include "kmem.h"
#include "io.h"
#include "defs.h"

#define VGA_BUFFER 0xB8000 + KERN_VIRT_BASE
#define ROWS 25
#define COLS 80

#define FCOLOR COLOR_WHITE
#define BCOLOR COLOR_BLACK

static uint32_t col = 0;
static uint32_t row = 0;

uint16_t* vga_buffer = (void*)VGA_BUFFER;

void update_cursor(int row, int col)
{
	unsigned short position=(row*80) + col;

	// cursor LOW port to vga INDEX register
	outb(0x3D4, 0x0F);
	outb(0x3D5, (unsigned char)(position&0xFF));
	// cursor HIGH port to vga INDEX register
	outb(0x3D4, 0x0E);
	outb(0x3D5, (unsigned char )((position>>8)&0xFF));
}

char hex[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};

static inline uint16_t genChar(char c, uint8_t fore_color, uint8_t back_color) {
	return c | ((fore_color | back_color <<4)<< 8);
}

void kputchar_color(char c, uint8_t fcolor, uint8_t bcolor, bool ucursor) {
	if (c == '\n') {
		uint16_t* address = vga_buffer + (row*COLS)+col;
		*address = genChar('\n', BCOLOR, BCOLOR);
		row++;
		col = 0;
	} else if (c == '\r') {
		col = 0;
	} else if (c == '\t') {
		col += 4 - (col % 4);
	} else if (c == '\b') {
		if (col == 0) {
			if (row > 0) {
				row--;
				uint32_t i = 0;
				for (i  = COLS-1; i >= 0; i--) {
					uint16_t* address = vga_buffer + (row*COLS)+i;
					if ((*address & 0xF) == '\n') {
						col = i;
						i = 50;
						break;
					}
				}
				if (i == 0) {
					col = COLS-1;
				}
			}
		} else {
			col--;
		}
		uint16_t* address = vga_buffer + (row*COLS)+col;
		*address = genChar(' ', FCOLOR, BCOLOR);
	} else {
		uint16_t* address = vga_buffer + (row*COLS) + col;
		*address = genChar(c, fcolor, bcolor);
		col++;
	}

	// handle overflow:
	if (col > COLS-1) {
		col = 0;
		row++;
	}
	if (row > ROWS-1) {
		for (uint32_t r = 0; r < ROWS-1; r++) {
			// copy row r+1 to row r
			for (uint32_t c = 0; c < COLS; c++) {
				uint16_t* address = vga_buffer + (r*COLS)+c;
				*address = *(vga_buffer + ((r+1)*COLS)+c);
			}
		}
		for (uint32_t c = 0; c < COLS; c++) {
			uint16_t* address = vga_buffer + ((ROWS-1)*COLS)+c;
			*address = genChar(' ', FCOLOR, BCOLOR);
		}
		row = ROWS-1;
	}

	if (ucursor)
		update_cursor(row, col);
}

void kputchar(char c) {
	kputchar_color(c, FCOLOR, BCOLOR, true);
}

void kputs_color(char* string, uint8_t fcolor, uint8_t bcolor) {
	char* c = string;
	while(*c != '\0') {
		kputchar_color(*c, fcolor, bcolor, false);
		c++;
	}
	update_cursor(row, col);
}

void kputs(char*string) {
	kputs_color(string, FCOLOR, BCOLOR);
}

void print_hex(uint32_t val) {
	char tmp[9] = {[0 ... 8] = '0'};
	tmp[8]  = '\0';
	uint32_t i = 7;
	while(val != 0) {
		char first = hex[val & 	0xF];
		tmp[i] = first;
		i--;
		val >>= 4;
	}
	
	kputs("0x");
	kputs(tmp);
}

void print_dec(uint32_t val) {
	char tmp[12] = {[0 ... 11] = '0'};
	tmp[11] = 0;
	uint32_t i = 10;
	while (val != 0) {
		char first = hex[val % 10];
		tmp[i] = first;
		i--;
		val /= 10;
	}

	kputs(tmp+i+1);
}

void cls(){
	uint32_t i = 0;
	for (i = 0; i < ROWS*COLS*2; i+=2) {
		*((uint16_t*)((uint8_t*)VGA_BUFFER+i)) = genChar(' ', FCOLOR, BCOLOR);
	}
	row = 0;
	col = 0;
	update_cursor(row, col);
}

void panic(char* error) {
	kputs_color(error, COLOR_RED, COLOR_BLACK);
	while(1);
}
