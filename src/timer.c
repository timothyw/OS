#include "idt.h"
#include "io.h"
#include "display.h"

void timer_handler(registers_t regs) {
	//kputs("tick\n");
}

void init_timer(uint32_t freq) {
	register_interrupt_handler(0x20, &timer_handler);

	uint32_t divisor = 1193180 / freq;

	outb(0x43, 0x36); // command byte

	uint8_t l = (uint8_t)(divisor & 0xFF);
	uint8_t h = (uint8_t)((divisor >> 8) & 0xFF);
	outb(0x40, l);
	outb(0x40, h);
}
