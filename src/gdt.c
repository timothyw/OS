#include "gdt.h"

gdt_entry_t gdt_entries[3] = {
	{0, 0, 0, 0, {0}, 0},			// null segment
	{0xFFFF, 0, 0, 0x9A, {0xF, 0xC}, 0},	// code segment
	{0xFFFF, 0, 0, 0x92, {0xF, 0XC}, 0}	// data segment
};

gdt_ptr_t gdt_ptr = {.size=sizeof(gdt_entry_t)*3 - 1, .offset = &gdt_entries[0]};
