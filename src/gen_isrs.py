#!/usr/bin/python2

# isr_defs.h -> extern void isrx();
# idt_defs.h -> idt_entries[x] = ...;
# isr_asm.inc -> ISR_NOERRCODE...

tmp1 = ""
tmp2 = ""
tmp3 = ""


for i in range(0, 256):
    tmp1 += "extern void isr" + str(i) + "();\n"
    tmp2 += "idt_entries[{0}] = (idt_entry_t){{GET_OFFSET_1(isr{0}), 0x08, 0, 0x8E, GET_OFFSET_2(isr{0})}};\n".format(i)
    tmp3 += "ISR_NOERRCODE {0}\n".format(i)

print(tmp1)
print(tmp2)
print(tmp3)

f = open("isr_defs.h", "w")
f.write(tmp1)
f.close();

f = open("idt_defs.h", "w")
f.write(tmp2)
f.close();

f = open("isr_defs.inc", "w")
f.write(tmp3)
f.close();
