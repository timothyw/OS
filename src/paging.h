#ifndef PAGING_H
#define PAGING_H

#include <stdint.h>

#include "multiboot.h"

typedef struct page {
        uint32_t present    : 1;
        uint32_t rw         : 1;
        uint32_t user       : 1;
        uint32_t writet     : 1;
        uint32_t cache      : 1;
        uint32_t accessed   : 1;
        uint32_t zero       : 1;
        uint32_t size       : 1;
        uint32_t ignored    : 1;
        uint32_t unused     : 13;
        uint32_t frame      : 10;
} page_t;
#define GET_PAGE(x) ((x) >> 22)
#define PAGE_ALIGN(x) ((x) & 0xFFC00000)

void map_page(uint32_t phys_page, uint32_t virt_page);
void init_memory(multiboot_info_t* mbh);

#endif

