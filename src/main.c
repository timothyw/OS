#include <stdint.h>

#include "paging.h"
#include "display.h"
#include "defs.h"
#include "multiboot.h"
#include "timer.h"
#include "keyboard.h"

extern uint32_t end;



int main(multiboot_info_t* multiboot_header) {
	if (!((multiboot_header->flags & MULTIBOOT_INFO_MEMORY) && 
		(multiboot_header->flags & MULTIBOOT_INFO_MEM_MAP))) {
		cls();
		panic("no memory info in multiboot!!!!");
		return 0;
	}

	cls();
	kputs("flags: "); print_hex(multiboot_header->flags); kputs("\n");
	kputs("mem_size:\n");
	kputs("mem_low: "); print_hex(multiboot_header->mem_lower); kputs("\n");
	kputs("mem_up: "); print_hex(multiboot_header->mem_upper); kputs("\n");
	kputs("cmdline: "); kputs((char*)multiboot_header->cmdline); kputs("\n");

	init_memory(multiboot_header);
	init_timer(20);
	init_keyboard();

	kputs("\n\n enter something: ");
	char * str = getsn();
	kputs("\n you entered: ");
	kputs(str);
	kputs("\n");
	return 0;
}
