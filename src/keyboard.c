#include "idt.h"
#include "io.h"
#include "display.h"

#define NO 0x0

char keymap[128] = {
  NO,   0x1B, '1',  '2',  '3',  '4',  '5',  '6',  // 0x00
  '7',  '8',  '9',  '0',  '-',  '=',  '\b', '\t',
  'q',  'w',  'e',  'r',  't',  'y',  'u',  'i',  // 0x10
  'o',  'p',  '[',  ']',  '\n', NO,   'a',  's',
  'd',  'f',  'g',  'h',  'j',  'k',  'l',  ';',  // 0x20
  '\'', '`',  NO,   '\\', 'z',  'x',  'c',  'v',
  'b',  'n',  'm',  ',',  '.',  '/',  NO,   '*',  // 0x30
  NO,   ' ',  NO,   NO,   NO,   NO,   NO,   NO,
  NO,   NO,   NO,   NO,   NO,   NO,   NO,   '7',  // 0x40
  '8',  '9',  '-',  '4',  '5',  '6',  '+',  '1',
  '2',  '3',  '0',  '.',  NO,   NO,   NO,   NO,   // 0x50
};

#define BUF_SIZE 256

char in_buffer[BUF_SIZE];
volatile uint32_t start_ptr = 0;
volatile uint32_t end_ptr = 0;

void add_buffer(char c) {
	if (end_ptr == ((start_ptr -1 + BUF_SIZE) % BUF_SIZE))
		return;
	in_buffer[end_ptr] = c;
	end_ptr = (end_ptr+1)%BUF_SIZE;
}

char get_buffer() {
	if (start_ptr == end_ptr)
		return '\0';
	char ret = in_buffer[start_ptr];
	start_ptr = (start_ptr+1)%BUF_SIZE;
	return ret;
}

void back_buffer() {
	if (start_ptr == end_ptr)
		return;
	end_ptr = (end_ptr -1 + BUF_SIZE) % BUF_SIZE;
}

void keyboard_handler(registers_t regs) {
	uint8_t in = inb(0x60);
	if (in & 0x80) {

	} else {
		char c = keymap[in];

		if (c == '\b') {
			if (start_ptr != end_ptr) {
				back_buffer();
				kputchar(c);
			}
		} else {
			add_buffer(c);
			kputchar(c);
		}
		// check \b
		//	-> if inbuf isn't empty:
		//	 	-> remove char from inbuf
		//		-> print
		// else:
		//	-> add to buf
		//	-> print
	}
}

char getchar() {
	// read first char from inbuf
	// return
	while (start_ptr == end_ptr) {
		asm volatile ("hlt");
	};
	return get_buffer();
}

char sn_buffer[BUF_SIZE];

char* getsn() {
	for (uint32_t i = start_ptr; i != end_ptr; i = (i + 1) % BUF_SIZE) {
		if (in_buffer[i] == '\n') {
			// copy to sn_buffer
			// return sn_buffer
			// clear buffer!!!
			uint32_t k = 0;
			for (uint32_t j = start_ptr; j != i; j = (j+1) % BUF_SIZE) {
				sn_buffer[k] = in_buffer[j];
				k++;
			}
			sn_buffer[k] = '\0';
			start_ptr = i;
			return sn_buffer;
		}
	}
	// read end_ptr until '\n' found
	while (in_buffer[(end_ptr-1+BUF_SIZE) % BUF_SIZE] != '\n') {
		asm volatile("hlt");
	}
	// read from start to end -> return
	// clear buffer!!
	uint32_t k = 0;
	for (uint32_t j = start_ptr; j != end_ptr; j = (j+1) % BUF_SIZE) {
		sn_buffer[k] = in_buffer[j];
		k++;
	}
	sn_buffer[k] = '\0';
	start_ptr = end_ptr = 0;
	return sn_buffer;
}

void init_keyboard() {
	register_interrupt_handler(0x21, &keyboard_handler);
}
