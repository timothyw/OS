#ifndef GDT_H
#define GDT_H

#include <stdint.h>

typedef struct granu {
	uint8_t limit_mid : 4;
	uint8_t flags : 4;
} granu_t;

typedef struct gdt_ptr {
	uint16_t size;
	struct gdt_entry* offset;
} __attribute__((packed)) gdt_ptr_t;

typedef struct gdt_entry {
	uint16_t limit_low;
	uint16_t base_low;      
	uint8_t  base_middle;   
	uint8_t  access;
	granu_t  granularity;
	uint8_t  base_high;
} __attribute__((packed)) gdt_entry_t;

#endif
