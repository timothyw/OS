#include <stdint.h>
#include "kmem.h"

void* memset(void* ptr, uint8_t val, uint32_t len) {
	void* p = ptr;
	for(;(uint32_t)(p-ptr) < len;p++) {
		*((uint8_t*)p) = val;
	}
	return ptr;
}

void* memcpy(void* dest, const void* src, uint32_t len) {
	uint32_t i;
	for (i=0; i <= len; i++) {
		*((uint8_t*)dest+i) = *((uint8_t*)src);
	}
	return dest;
}

char* strcpy(char* dest, const char*src) {
	uint32_t i =  0;
	for(i = 0; src[i] != '\0'; i++){
		dest[i] = src[i];
	}
	dest[i] = 0;
	return dest;
}


uint32_t strlen(char*str) {
	uint32_t i = 0;
	for(i=0;str[i] != '\0'; i++);
	return i+1;
}
